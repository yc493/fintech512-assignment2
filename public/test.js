function selectChanged() {
    var select = document.getElementById("stock-select");
    var selectedOption = select.options[select.selectedIndex].value;
    var contentContainer = document.getElementById("contentContainer");
    
    switch (selectedOption) {
      case "dog":
        contentContainer.innerHTML = "You selected Option 1";
        break;
      case "cat":
        contentContainer.innerHTML = "You selected Option 2";
        break;
      case "hamster":
        contentContainer.innerHTML = "You selected Option 3";
        break;
      default:
        contentContainer.innerHTML = "Please select an option";
    }
  }
  
 