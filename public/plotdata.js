function selectChanged() {
    var select = document.getElementById("stock-select");
    var selectedOption = select.options[select.selectedIndex].value;
	var stockname = select.options[select.selectedIndex].label;
    var contentContainer = document.getElementById("contentContainer");
    
    switch (selectedOption) {
      case "AAPL":
        makeplot("https://gitlab.oit.duke.edu/yc493/fintech512-assignment2/-/raw/main/AAPL.csv", stockname);
        break;
      case "GME":
		makeplot("https://gitlab.oit.duke.edu/yc493/fintech512-assignment2/-/raw/main/GME.csv", stockname);
        break;
      case "TSLA":
        makeplot("https://gitlab.oit.duke.edu/yc493/fintech512-assignment2/-/raw/main/TSLA.csv", stockname);
        break;
	  case "SPY":
		makeplot("https://gitlab.oit.duke.edu/yc493/fintech512-assignment2/-/raw/main/SPY.csv", stockname);
		break;
      default:
       // contentContainer.innerHTML = "Please select an option";
    }
  };
  

function makeplot(url, stockname) {
	d3.csv(url, function (data) { processData(data, stockname) });

};

function processData(allRows, stockname) {

	console.log(allRows);
	var x = [], y = [];
	for (var i = 0; i < allRows.length; i++) {
		row = allRows[i];
		x.push(row['Date']);
		y.push(row['Close']);
	}
	console.log('X', x, 'Y', y);
	makePlotly(x, y, stockname);
}

function makePlotly(x, y, stockname) {
	var plotDiv = document.getElementById("tester");
	var traces = [{
		x: x,
		y: y
	}];

	Plotly.newPlot('tester', traces,
		{ title: stockname });
};

function alertFunction() {
    confirm('This is the default confirm!', function(result) { 
		console.log('This was logged in the callback: ' + result); 
	});
};